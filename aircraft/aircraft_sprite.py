import random

import pygame

WINDOW_RECT = pygame.Rect(0, 0, 480, 700)

EMEMY_ID = pygame.USEREVENT

HERO_FIRE_EVENT = pygame.USEREVENT + 1


class aircraft_sprite(pygame.sprite.Sprite):

    def __init__(self, image_name, speed=1):
        super().__init__()
        self.image = pygame.image.load(image_name)
        self.rect = self.image.get_rect()
        self.speed = speed

    def update(self):
        self.rect.y += self.speed


class bg_sprite(aircraft_sprite):
    def __init__(self, is_two=False):
        super().__init__("../images/background.png")
        if is_two:
            print("true")
            self.rect.y = -self.rect.height

    def update(self):
        super().update()
        if self.rect.y >= WINDOW_RECT.height:
            self.rect.y = -self.rect.height


class enemy_sprite(aircraft_sprite):
    def __init__(self):
        super().__init__("../images/enemy1.png")
        self.speed = random.randint(1, 3)
        self.rect.bottom = 0
        max_x = WINDOW_RECT.width - self.rect.width
        self.rect.x = random.randint(0, max_x)

    def update(self):
        super().update()
        if self.rect.y >= WINDOW_RECT.height:
            self.kill()


class hero(aircraft_sprite):
    def __init__(self):
        super().__init__("../images/me1.png", 0)
        self.rect.centerx = WINDOW_RECT.centerx
        self.rect.bottom = WINDOW_RECT.bottom - 120
        self.bullet_group = pygame.sprite.Group()

    def update(self):
        self.rect.x += self.speed
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.right > WINDOW_RECT.right:
            self.rect.right = WINDOW_RECT.right

    def fire(self):
        for i in (0, 1, 2):
            self.bullet = bullet()
            self.bullet.rect.center = self.rect.center
            self.bullet.rect.bottom = self.rect.top + i * 20
            self.bullet_group.add(self.bullet)


class bullet(aircraft_sprite):
    def __init__(self):
        super().__init__("../images/bullet2.png", -2)

    def update(self):
        super().update()
        if self.rect.y < 0:
            self.kill()
