import socket

udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
udp.bind(('', 12345))
ip = input("请输入对方的IP")
try:
    port = int(input("请输入端口号"))
except Exception:
    print("请输入正常格式")
sendData = input("请输入发送内容")
udp.sendto(sendData.encode("gbk"), (ip, port))

recv_data = udp.recvfrom(1024)
msg = recv_data[0]
print(msg.decode("gbk"))
udp.close()
