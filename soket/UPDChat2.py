import socket

# udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# udp.bind(('', 6666))
import threading


def send(udp, ip, port):
    while True:
        msg = input("请输入发送的数据")
        udp.sendto(msg.encode("gbk"), (ip, int(port)))


def recv_msg(udp):
    while True:
        recv_msg = udp.recvfrom(1024)
        print("%s发送： %s " % (str(recv_msg[1]), str(recv_msg[0].decode("gbk"))))


# while True:
#     send_msg(udp)
#     recv_msg(udp)

if __name__ == '__main__':
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.bind(('', 6666))
    ip = input("请输入ip")
    prot = input("请输入端口号")
    t1 = threading.Thread(target=send, args=(udp,ip,prot,))
    t2 = threading.Thread(target=recv_msg,args=(udp,))
    t1.start()
    t2.start()
