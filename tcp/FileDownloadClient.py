import socket


def main():
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # server_ip = input("请输入服务器IP")
    server_ip = "192.168.239.1"
    # server_port = int(input("请输入服务器端口"))
    server_port = 6688
    tcp_socket.connect((server_ip, server_port))
    while True:
        downloadFileName = input("请输入要下载的文件名")
        tcp_socket.send(downloadFileName.encode("gbk"))
        recv_data = tcp_socket.recv(1024).decode("gbk")
        if recv_data != "1":
            with open("[新]" + downloadFileName, "wb") as f:
                f.write(recv_data)
        else:
            continue

    tcp_socket.close()


if __name__ == "__main__":
    main()
