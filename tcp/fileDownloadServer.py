import socket


def main():
    tcp_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_server.bind(('', 6688))
    print(tcp_server)
    tcp_server.listen(128)
    new_socket, clientr_addr = tcp_server.accept()
    while True:
        fileName = new_socket.recv(1024).decode("gbk")
        try:
            file = open(fileName, "rb")
            new_socket.send(file.read())
            file.close()
        except Exception as e:
            print("没有这个文件")
            new_socket.send("1".encode("gbk"))

if __name__ == "__main__":
    main()
