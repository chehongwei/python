import socket


def main():
    tcp_client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_client.connect(("192.168.239.1", 8080))
    send_data = input("请输入发送的内容")
    tcp_client.send(send_data.encode("gbk"))
    tcp_client.close()


if __name__ == "__main__":
    main()
