import multiprocessing
import threading
import time

g_num = 0


def test1():
    global g_num
    print("1")


def test2():
    global g_num
    print("2")


def main():
    m1 = multiprocessing.Process(target=test1)
    m2 = multiprocessing.Process(target=test2)
    m1.start()
    m2.start()
    # t1 = threading.Thread(target=test1, args=(1000000,))
    # t2 = threading.Thread(target=test2, args=(1000000,))
    # t1.start()
    # t2.start()
    print(g_num)
    queue = multiprocessing.queues


if __name__ == '__main__':
    main()
